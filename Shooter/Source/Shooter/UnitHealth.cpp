// Fill out your copyright notice in the Description page of Project Settings.

#include "UnitHealth.h"


// Sets default values for this component's properties
UUnitHealth::UUnitHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	this->MaxHP = FMath::RandRange(this->RandomMin, this->RandomMax);
	// ...
}


void UUnitHealth::TakeDamage(float damage){
	this->CurHP -= damage;
}

void UUnitHealth::Heal(float health){
	this->CurHP += health;
	if (this->CurHP > this->MaxHP)
		this->CurHP = this->MaxHP;
}

float UUnitHealth::NormalizeHealth()
{
	return this->MaxHP/this->CurHP;
}

bool UUnitHealth::IsAlive()
{
	return NormalizeHealth() > 0.0f;
}

/*void UUnitHealth::CalledFromCPP()
{

}*/

// Called when the game starts
void UUnitHealth::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUnitHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

