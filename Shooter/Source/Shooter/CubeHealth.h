// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CubeHealth.generated.h"

UCLASS()
class SHOOTER_API ACubeHealth : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACubeHealth();
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	int32 RandomMin;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	int32 RandomMax;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	int32 MaxHP;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	int32 CurHP;
		UFUNCTION(BluePrintCallable, Category = "Damage")
	void SetHealth();
		UFUNCTION(BluePrintCallable, Category = "Damage")
	void TakeDamage(int damage);
		UFUNCTION(BluePrintCallable, Category = "Damage")
	void CheckHealth();
		UFUNCTION(BlueprintImplementableEvent, Category = "Damage")
	void CalledFromCpp();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
