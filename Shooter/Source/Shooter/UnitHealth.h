// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UnitHealth.generated.h"

// ClassGroup=(Custom), meta=(BlueprintSpawnableComponent)
UCLASS( )
class SHOOTER_API UUnitHealth : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUnitHealth();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float RandomMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float RandomMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float CurHP;
	
	UFUNCTION(BluePrintCallable, Category = "Damage")
		void TakeDamage(float damage);
	UFUNCTION(BluePrintCallable, Category = "Damage")
		void Heal(float health);
	UFUNCTION(BluePrintCallable, Category = "Damage")
		float NormalizeHealth();
	UFUNCTION(BluePrintCallable, Category = "Damage")
		bool IsAlive();



protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
