// Fill out your copyright notice in the Description page of Project Settings.

#include "CubeHealth.h"


// Sets default values
ACubeHealth::ACubeHealth()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
}


void ACubeHealth::SetHealth()
{
	this->MaxHP = FMath::RandRange(this->RandomMin, this->RandomMax);
	this->CurHP = MaxHP;
}

void ACubeHealth::TakeDamage(int damage){
	this->CurHP -= damage;
	if (this->CurHP <= 0)
		CalledFromCpp();
}

void ACubeHealth::CheckHealth(){
	if (this->CurHP <= 0)
		Destroy();
}


// Called when the game starts or when spawned
void ACubeHealth::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACubeHealth::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

